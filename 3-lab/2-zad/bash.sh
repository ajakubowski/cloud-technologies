#!/bin/bash

DOCKER_FILE="Dockerfile"
IMAGE_NAME="nginx"
CONTAINER_NAME="nginx_2"

FILE_TO_COPY="default.conf"
PORT_TO_LISTEN="2763"


cat << EOF > $FILE_TO_COPY
server {
    listen       $PORT_TO_LISTEN;
    listen  [::]:$PORT_TO_LISTEN;
    server_name  localhost;

    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}
EOF

if [[ "$(docker images -q $IMAGE_NAME 2> /dev/null)" == "" ]]; then
    echo "Pulling $IMAGE_NAME image..."    
    docker pull $IMAGE_NAME
else
    echo "Required image $IMAGE_NAME is already downloaded"
fi

if [[ $(docker ps -a --format '{{.Names}}' | grep -w $CONTAINER_NAME) ]]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

docker run --name $CONTAINER_NAME -p 8080:$PORT_TO_LISTEN -d $IMAGE_NAME
docker cp $FILE_TO_COPY $CONTAINER_NAME:/etc/nginx/conf.d/
docker restart $CONTAINER_NAME
