#!/bin/bash

DOCKER_FILE="Dockerfile"
IMAGE_NAME="nginx"
CONTAINER_NAME="nginx_1"

FILE_TO_COPY="index.html"

if [[ "$(docker images -q $IMAGE_NAME 2> /dev/null)" == "" ]]; then
    echo "Pulling $IMAGE_NAME image..."    
    docker pull $IMAGE_NAME
else
    echo "Required image $IMAGE_NAME is already downloaded"
fi

if [[ $(docker ps -a --format '{{.Names}}' | grep -w $CONTAINER_NAME) ]]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

docker run --name $CONTAINER_NAME -p 8080:80 -d $IMAGE_NAME
docker cp $FILE_TO_COPY $CONTAINER_NAME:/usr/share/nginx/html