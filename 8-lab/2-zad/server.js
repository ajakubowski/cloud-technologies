const express = require('express');
const Redis = require('ioredis');

const app = express();
const port = 3000;

// Utwórz połączenie z bazą danych Redis
const redis = new Redis({
  host: 'db', // nazwa kontenera Redis w sieci Docker Compose
  port: 6379 // domyślny port Redis
});

// Trasa dla strony głównej
app.get('/', (req, res) => {
  res.send('Server is running. Use /add and /get endpoints.');
});

// Dodaj rekord do bazy danych Redis
app.get('/add', async (req, res) => {
  const { key, value } = req.query;

  if (!key || !value) {
    return res.status(400).send('Missing key or value');
  }

  await redis.set(key, value);

  return res.send('Record added to Redis');
});

// Odczytaj rekord z bazy danych Redis
app.get('/get', async (req, res) => {
  const { key } = req.query;

  if (!key) {
    return res.status(400).send('Missing key');
  }

  const value = await redis.get(key);

  if (!value) {
    return res.status(404).send('Record not found');
  }

  return res.send(`Value for key ${key}: ${value}`);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
