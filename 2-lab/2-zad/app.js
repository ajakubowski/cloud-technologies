const PORT = 8080;

const express = require('express');
const app = express();

app.get('/', (req, res) => {
  const currentDate = new Date();
  
  const data = {
    date: currentDate.toDateString(),
    time: currentDate.toLocaleTimeString()
  };

  res.json(data);
});

app.listen(PORT, () => {
  console.log(`Serwer Express działa na porcie ${PORT}`);
});
