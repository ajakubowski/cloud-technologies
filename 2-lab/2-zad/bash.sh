#!/bin/bash

# make dockerfile
DOCKER_FILE="Dockerfile"
IMAGE_NAME="node:14"

if [[ "$(docker images -q $IMAGE_NAME 2> /dev/null)" == "" ]]; then
    echo "Pulling $IMAGE_NAME image..."    
    docker pull $IMAGE_NAME:$TAG
else
    echo "Required image $IMAGE_NAME is already downloaded"
fi

echo "FROM $IMAGE_NAME" > $DOCKER_FILE
echo "RUN npm install" >> $DOCKER_FILE
echo "WORKDIR /usr/src/" >> $DOCKER_FILE
echo "COPY app.js package.json package-lock.json /usr/src/" >> $DOCKER_FILE
echo "EXPOSE 8080" >> $DOCKER_FILE
echo "CMD [\"npm\", \"start\"]" >> $DOCKER_FILE

CONTAINER_NAME="node_14"

if [[ $(docker ps -a --format '{{.Names}}' | grep -w $CONTAINER_NAME) ]]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

docker build -t $IMAGE_NAME .
docker run -d -p 8080:8080 --name $CONTAINER_NAME $IMAGE_NAME
