#!/bin/bash

# make dockerfile
DOCKER_FILE="Dockerfile"
IMAGE_NAME="node:16"
CONTAINER_NAME="node_16"


if [[ "$(docker images -q $IMAGE_NAME 2> /dev/null)" == "" ]]; then
    echo "Pulling $IMAGE_NAME image..."    
    docker pull $IMAGE_NAME:$TAG
else
    echo "Required image $IMAGE_NAME is already downloaded"
fi

cat << EOF > Dockerfile
FROM $IMAGE_NAME
WORKDIR /usr/src/
COPY app.js package*.json
RUN npm install
EXPOSE 8080
CMD ["node", "app.js"]
EOF



# Tworzymy plik docker-compose.yml
cat << EOF > docker-compose.yml
version: '3'
services:
  app:
    container_name: $CONTAINER_NAME
    restart: no
    build: .
    ports: 
      - '8080:8080'
    external_links:
      - mongo
    networks:
      - mongoNetwork2
  mongo:
    container_name: mongo
    image: mongo
    volumes:
      - ./data:/data/db
    ports:
      - '27017:27017'
    networks:
      - mongoNetwork
EOF


if [[ $(docker ps -a --format '{{.Names}}' | grep -w $CONTAINER_NAME) ]]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

docker-compose build
docker-compose up -d