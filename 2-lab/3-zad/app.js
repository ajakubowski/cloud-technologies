const express = require('express');
const mongoose = require('mongoose');

// Zmieniamy adres połączenia na nazwę kontenera MongoDB
mongoose.connect('mongodb://mongo:27017/db');

const mySchema = new mongoose.Schema({
  name: String,
  age: Number
});

const MyModel = mongoose.model('MyModel', mySchema);

const app = express();
const port = 8080;

async function addInitialRecords() {
    try {
      await MyModel.create([
        { name: 'John', age: 30 },
        { name: 'Alice', age: 25 }
      ]);
    } catch (err) {
      console.error('Err:', err);
    }
}  

addInitialRecords()

app.get('/', async (req, res) => {
  try {
    const data = await MyModel.find();
    res.json(data);
  } catch (err) {
    console.error('Błąd:', err);
    res.status(500).send('Wystąpił błąd');
  }
});

app.listen(port, () => {
  console.log(`Serwer nasłuchuje na porcie ${port}`);
});
