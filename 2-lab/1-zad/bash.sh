#!/bin/bash

# make dockerfile
DOCKER_FILE="Dockerfile"
IMAGE_NAME="node:12"

if [[ "$(docker images -q $IMAGE_NAME 2> /dev/null)" == "" ]]; then
    echo "Pulling $IMAGE_NAME image..."    
    docker pull $IMAGE_NAME:$TAG
else
    echo "Required image $IMAGE_NAME is already downloaded"
fi

echo "FROM $IMAGE_NAME" > $DOCKER_FILE
echo "RUN npm install" >> $DOCKER_FILE
echo "WORKDIR /usr/src/" >> $DOCKER_FILE
echo "COPY app.js /usr/src/" >> $DOCKER_FILE
echo "EXPOSE 8080" >> $DOCKER_FILE
echo "CMD [\"node\", \"app.js\"]" >> $DOCKER_FILE

CONTAINER_NAME="node_12"

if [[ $(docker ps -a --format '{{.Names}}' | grep -w $CONTAINER_NAME) ]]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

docker build -t $IMAGE_NAME .
docker run -d -p 8080:8080 --name $CONTAINER_NAME $IMAGE_NAME
docker exec -it $CONTAINER_NAME node /usr/src/app.js